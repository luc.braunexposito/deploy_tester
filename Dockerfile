FROM --platform=linux/amd64 quay.io/libpod/ubuntu:latest as builder


RUN groupadd -r swuser -g 433 && \
    useradd -u 431 -r -g swuser -s /sbin/nologin -c "Docker image user" swuser
USER root

WORKDIR /python-docker
RUN apt-get update && apt-get install -y curl python3 python3-pip
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
COPY . .
EXPOSE 5000
RUN ["pytest", "tests/"]

USER swuser
ENTRYPOINT ["python3","app.py"]