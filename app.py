#!/usr/bin/env python3
from flask import Flask

app = Flask(__name__)

@app.route('/')
def home():
    return "Magnifique page d'accueil"


@app.route('/about')
def about():
    return 'This is the about page'

if __name__ == '__main__':
    app.run(debug=True, port=5000, host='0.0.0.0')