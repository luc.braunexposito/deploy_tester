#!/usr/bin/env python3
from flask import Flask

def test_home_msg():
    app = Flask(__name__)
    add_route(app)
    client = app.test_client()
    response = client.get('/')
    assert response.get_data() == b"Magnifique page d'accueil"

def test_about_msg():
    app = Flask(__name__)
    add_route(app)
    client = app.test_client()
    response = client.get('/about')
    assert response.get_data() == b"This is the about page"

def add_route(app):
    @app.route('/')
    def home():
        return "Magnifique page d'accueil"
    @app.route('/about')
    def about():
        return 'This is the about page'